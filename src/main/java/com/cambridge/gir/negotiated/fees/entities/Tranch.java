package com.cambridge.gir.negotiated.fees.entities;

import lombok.Data;

@Data
public class Tranch {

    private Double from;
    private Double to;
    private Double percentage;

    public Tranch(){}

    public Tranch(Double from, Double to, Double percentage) {
        this.from = from;
        this.to = to;
        this.percentage = percentage;
    }

}
