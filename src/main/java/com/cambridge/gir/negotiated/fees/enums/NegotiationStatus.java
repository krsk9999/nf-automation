package com.cambridge.gir.negotiated.fees.enums;

public enum NegotiationStatus {

    Active ("Active"),
    Pending ("Pending"),
    Closed ("Closed"),
    Unsuccessful ("Unsuccessful");

    private final String negotiationStatus;

    NegotiationStatus(String eligibleClient) {
        this.negotiationStatus = eligibleClient;
    }

    public String get() {
        return negotiationStatus;
    }
}
