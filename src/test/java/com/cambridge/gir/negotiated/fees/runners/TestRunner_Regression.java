package com.cambridge.gir.negotiated.fees.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.TestNGCucumberRunner;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.runner.RunWith;
import org.testng.annotations.Test;
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/com/cambridge/gir/negotiated/fees/features/Negotiations.feature"
        ,format = {"json:target/cucumber1.json", "html:target/site/cucumber-pretty"}
        ,tags = {"@Regression"}
        ,glue = {"com.cambridge.gir.negotiated.fees.runners","src/test/java/com/cambridge/gir/negotiated/fees/steps",""}
)
public class TestRunner_Regression extends AbstractTestNGCucumberTests {
        /**
         * Create one test method that will be invoked by TestNG and invoke the
         * Cucumber runner within that method.
         */
        @Test(groups = "fr", description = "Example of using TestNGCucumberRunner to invoke Cucumber")
        public void runCukes() {

                //new TestNGCucumberRunner(getClass()).runCukes();
        }
}
