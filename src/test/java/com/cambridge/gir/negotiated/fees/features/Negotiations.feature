@Features
Feature: Negotiation (Create/Update/Delete)
  This feature is intended to test the searching functionality

  @Regression @GlobalSearch
  Scenario: Search for a Firm and validate some results are retrieved
    Given I navigate to the Negotiated Fees main page
    When the user searches for a firm name
    Then The system retrieves the firms that properly match the search criteria

  @Regression
  Scenario: Create a new negotiation without Data
    Given I navigate to the Negotiated Fees main page
    When the user searches for a firm name
    And clicks Add the firm to add it to a Negotiation
    And the user clicks Add Negotiation
    Then the user saves the negotiation
    And the system shows a message indicating the negotiation was successfully saved

  @Regression
  Scenario: Create a new negotiation with "Negotiation Information"
    Given I navigate to the Negotiated Fees main page
    When the user searches for a firm name
    And clicks Add the firm to add it to a Negotiation
    And the user clicks Add Negotiation
    And the user enters all Negotiation Information
    Then the user saves the negotiation
    And the system shows a message indicating the negotiation was successfully saved

  @Regression @Test
  Scenario: Edit and save an existing negotiation
    Given I navigate to the Negotiated Fees main page
    When the user searches for a firm name
    And clicks Add the firm to add it to a Negotiation
    And a user clicks to edit a negotiation
    And the user edits the negotiation date
    Then the user saves the negotiation
    And the system shows a message indicating the negotiation was successfully saved

  @Regression
  Scenario: Delete an existing negotiation
    Given I navigate to the Negotiated Fees main page
    When the user searches for a firm name
    And clicks Add the firm to add it to a Negotiation
    And a user clicks to edit a negotiation
    Then the user clicks to delete a negotiation
    And the system shows a message indicating the negotiation was successfully deleted

  @Regression
  Scenario: Create a new negotiation with flat original fee information
    Given I navigate to the Negotiated Fees main page
    When the user searches for a firm name
    And clicks Add the firm to add it to a Negotiation
    And the user clicks Add Negotiation
    And the user enters all Negotiation Information
    And the user click to add a fee
    And the user indicates that is an Original Fee
    And the user selects a feetype and feestructure
    And the user enters the fee value
    Then the user saves the negotiation
    And the system shows a message indicating the negotiation was successfully saved
