package com.cambridge.gir.negotiated.fees.enums;

public enum NegotiationAttributes {

    NewLaunch ("New Launch"),
    InternalComments ("Internal Comments"),
    Optica ("Optica"),
    ClientList ("Client List"),
    NewManager ("New Manager");

    private final String negotiationAttributes;

    NegotiationAttributes(String negotiationAttributes) {
        this.negotiationAttributes = negotiationAttributes;
    }

    public String get() {
        return negotiationAttributes;
    }

}
