package com.cambridge.gir.negotiated.fees.steps;

import com.cambridge.gir.negotiated.fees.pages.Main.TestPage;
import com.gft.qa.automation.core.exceptions.UnknownConditionException;
import com.gft.qa.automation.core.utils.Log;
import cucumber.api.java.en.Given;
import org.junit.Assert;

public class TestSteps extends BaseSteps {

    TestPage tp;

    public TestSteps() {
        try{
            this.tp = new TestPage();
        }catch(Exception ex){
            Log.error(ex.getMessage());
        }

    }

    @Given("^I navigate to the Test page$")
    public void i_navigate_to_the_Test_page() {
        try {
            tp.Open();
        } catch(UnknownConditionException ex){
            Assert.fail(ex.getMessage());
            throw ex;
        }catch (Exception ex) {
            Assert.fail(ex.getMessage());
            Log.error("There was an error generating the URL.\n\r" + ex.getMessage());
            throw ex;
        }
    }
}
