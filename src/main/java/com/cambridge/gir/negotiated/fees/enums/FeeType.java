package com.cambridge.gir.negotiated.fees.enums;

public enum FeeType {

    INCENTIVEFEECARRIEDINTEREST ("Incentive Fee/Carried Interest"),
    MANAGEMENTFEE ("Management Fee"),
    REDEMPTIONFEE ("Redemption Fee"),
    SALESSUBSCRIPTIONCHARGE ("Sales/Subscription Charge"),
    SUBSEQUENTCLOSEINTEREST ("Subsequent Close interest"),
    TOTALEXPENSERATION ("Total Expense Ratio"),
    ORIGINALMANAGEMENTFEE ("Original Management Fee");

    private final String feeType;

    FeeType(String feeType) {
        this.feeType = feeType;
    }

    public String get() {
        return feeType;
    }
}
