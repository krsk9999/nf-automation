package com.cambridge.gir.negotiated.fees.steps;

import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.utils.Log;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriverException;

public class Hooks {

    @Before
    public void setUp(Scenario scenario) {
        GUtils.scenario = scenario;
        Log.startTestCase("Executing Scenario: " + scenario.getName());

    }

    @After
    public void cleanUp(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                scenario.embed(GUtils.getScreenShotToEmbed(), "image/png");
            } catch (WebDriverException wde) {
                Log.error(wde.getMessage());
                System.err.println(wde.getMessage());
            } catch (ClassCastException cce) {
                Log.error(cce.getMessage());
                cce.printStackTrace();
            } catch (Exception e) {
                Log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        Log.endTestCase("Completing Scenario: " + scenario.getName());
    }

}
