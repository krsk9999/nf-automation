package com.cambridge.gir.negotiated.fees.enums;

public enum EligibleClient {
    AllCaClients ("All CA Clients"),
    SpecificClientAndOrClientGroup ("Specific Client and/or Client Group"),
    ExcludeClientOrClientGroup ("Exclude Client or Client Group"),
    AllClientsIncludingNonCa ("All Clients (Including NON CA)");

    private final String eligibleClient;

    EligibleClient(String eligibleClient) {
        this.eligibleClient = eligibleClient;
    }

    public String get() {
        return eligibleClient;
    }
}
