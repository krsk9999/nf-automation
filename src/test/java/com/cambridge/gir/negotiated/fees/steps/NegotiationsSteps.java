package com.cambridge.gir.negotiated.fees.steps;

import com.cambridge.gir.negotiated.fees.entities.Negotiation;
import com.cambridge.gir.negotiated.fees.enums.*;
import com.cambridge.gir.negotiated.fees.pages.Main.HomePage;
import com.cambridge.gir.negotiated.fees.pages.Negotiation.NegotiationInformation;
import com.cambridge.gir.negotiated.fees.utils.NegotiationFactory;
import com.gft.qa.automation.core.exceptions.UnableToFindWindowException;
import com.gft.qa.automation.core.utils.Constant;
import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.utils.Log;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class NegotiationsSteps extends BaseSteps{

    private HomePage hp = new HomePage();
    private NegotiationInformation ni;
    private Negotiation negotiation;
    private String searchFirm;
    private Map<Integer, WebElement> previousNegotiationIds;

    public NegotiationsSteps() throws Exception {
        this.searchFirm = GUtils.properties.get(0).getProperty("defaultFirm");
        this.previousNegotiationIds = new HashMap<>();
    }

    @Given("^I navigate to the Negotiated Fees main page$")
    public void navigate() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        hp.Open();
    }

    @When("^the user searches for a firm name$")
    public void firmSearch() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        GUtils.SendKeys(hp.txtFirmSearchBox(), this.searchFirm);
        List<WebElement> search = hp.lstSearchResultSet();
        assertTrue(search.size() > 0);
    }

    @Then("^The system retrieves the firms that properly match the search criteria$")
    public void retrieveFirmSearchResults() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());

        List<WebElement> search = hp.lstSearchResultSet();
        assertTrue(search.size()>0);
    }

    @And("^clicks Add the firm to add it to a Negotiation$")
    public void addAssetToScreen() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());

        GUtils.ClickButton(hp.btnAddSearchResult());
        this.previousNegotiationIds = hp.getNegotiationIdsFromSummaryTable();
        assertTrue(hp.waitUntilNegotiationSummaryIsLoaded());
    }

    @And("^a user clicks to edit a negotiation$")
    public void userEditsNegotiation() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        Boolean found = false;
        WebElement selectedNegotiation = getFirstAvailableNegotiation();

        Log.info("Negotiation ID: " + this.previousNegotiationIds.entrySet().iterator().next().getKey());
        Log.info("Negotiation Status: " + selectedNegotiation.findElement(By.cssSelector(".badge")).getText());
        Log.info("Start Date: " + selectedNegotiation.findElement(By.cssSelector("td:nth-child(3)")).getText());
        Log.info("End Date: " + selectedNegotiation.findElement(By.cssSelector("td:nth-child(4)")).getText());

        GUtils.moveToElementByActions(selectedNegotiation);
        GUtils.ClickButton(hp.btnEditNegotiation(selectedNegotiation), "Edit Button");
        GUtils.scenario.embed(GUtils.getScreenShotToEmbed(), "image/png");
    }

    private WebElement getFirstAvailableNegotiation() {
        WebElement selectedNegotiation = null;
        for (Map.Entry<Integer, WebElement> entry : this.previousNegotiationIds.entrySet()) {
            if (!entry.getValue().findElement(By.cssSelector("td:nth-child(2)")).getText().equalsIgnoreCase("expired")) {
                selectedNegotiation = entry.getValue();
                break;
            }
        }
        return selectedNegotiation;
    }


    @And("^the user edits the negotiation date$")
    public void userModifiesNegotiationEndDate() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        if (this.ni == null) {
            this.ni = new NegotiationInformation();
        }

        LocalDate currentEndDate = ni.dpEndDate().getAttribute("value").equalsIgnoreCase("") ? LocalDate.now() : GUtils.convertStringToLocalDate(ni.dpEndDate().getAttribute("value")).plusYears(2L);

        ni.lstSelectNegotiationStatus(NegotiationStatus.Pending);
        GUtils.ClearInputDateField(ni.dpEndDate());
        GUtils.SendKeys(ni.dpEndDate(), GUtils.convertLocalDateToString(currentEndDate));
        ni.lstSelectNegotiationStatus(NegotiationStatus.Active);

        GUtils.scenario.embed(GUtils.getScreenShotToEmbed(), "image/png");
        GUtils.scenario.write("Hola pedazo!!!");
    }



    @And("^the user clicks Add Negotiation$")
    public void addNegotiation() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        if (hp.waitUntilNegotiationSummaryIsLoaded()) {
            GUtils.ClickButton(hp.btnAddNegotiation());
            Log.info("The Negotiation Summary Section was completely loaded");
        } else {
            Log.info("The Negotiation Summary Section was still loading after " + Constant.Waiting_For_Invisibility_Seconds + " Seconds.");
        }
        negotiation = NegotiationFactory.createNegotiationObject();
    }

    @And("^the user enters all Negotiation Information$")
    public void populateNegotiationInformation() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());

        this.ni = new NegotiationInformation();

        ni.lstSelectEligibleClient(negotiation.getEligibleClient());
        List<String> ec = new ArrayList<>();
        ec.add(negotiation.getEligibleClientsInput());
        ni.assignEligibleClient(ec);
        GUtils.ClickButton(ni.chkNewLaunch(), "New Launch");
        GUtils.SendKeys(ni.dpEffectiveDate(), negotiation.getEffectiveDate());
        GUtils.SendKeys(ni.dpEndDate(), negotiation.getEndDate());
        ni.lstSelectNegotiationStatus(negotiation.getNegotiationStatus());
        ni.chkNegotiationAttribute(NegotiationAttributes.InternalComments).click();
        ni.chkNegotiationAttribute(NegotiationAttributes.Optica).click();
        ni.chkNegotiationAttribute(NegotiationAttributes.ClientList).click();
        ni.chkNegotiationAttribute(NegotiationAttributes.NewManager).click();
        ni.assignNegotiator(negotiation.getNegotiator());
        GUtils.SendKeys(ni.txtActionRequiredToReceiveBenefit(), negotiation.getActionRequired());
        GUtils.SendKeys(ni.txtDetails(), negotiation.getDetails());
    }

    @Then("^the user saves the negotiation$")
    public void saveNegotiation() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        if (this.ni == null) {
            this.ni = new NegotiationInformation();
        }
        GUtils.ClickButton(ni.btnSave());
    }

    @And("^the system shows a message indicating the negotiation was successfully saved$")
    public void validateSuccesfullMessage() throws Throwable {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        assertTrue(hp.validateNegotiationSavedSuccessStatus().equals(StatusLevel.Success));
        hp.waitUntilNegotiationSummaryIsLoaded();
    }

    @Then("^the user clicks to delete a negotiation$")
    public void the_user_clicks_to_delete_a_negotiation() throws Exception {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        if (this.ni == null) {
            this.ni = new NegotiationInformation();
        }
        GUtils.ClickButton(ni.btnDelete());
        if (ni.waitForDeletionPrompt().isDisplayed()) {
            GUtils.ClickButton(ni.btnConfirmDelete());
        } else {
            throw new UnableToFindWindowException("The prompt to delete the negotiation was not displayed.");
        }
    }

    @And("^the system shows a message indicating the negotiation was successfully deleted$")
    public void the_system_shows_a_message_indicating_the_negotiation_was_successfully_deleted() throws Exception {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        assertTrue(hp.validateNegotiationDeletedSuccessStatus().equals(StatusLevel.Success));
        hp.waitUntilNegotiationSummaryIsLoaded();
    }

    @And("^the user indicates that is an Original Fee$")
    public void chkOriginalFee() throws Exception {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        GUtils.ClickButton(ni.chkOriginalFee(), "Original Fee");
    }

    @And("^the user click to add a fee$")
    public void lnkAddFee() throws Exception {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        GUtils.ClickButton(ni.btnAddFee());
    }

    @And("^the user selects a feetype and feestructure$")
    public void lstFeeTypeAndFeeStructure() throws Exception {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        ni.lstSelectFeeType(FeeType.MANAGEMENTFEE);
        ni.lstSelectFeeStructure(FeeStructure.Flat);
    }

    @And("^the user enters the fee value$")
    public void txtFeeValueEntered() throws Exception {
        Log.info("Executing Testing Method: " + new Object() {
        }.getClass().getEnclosingMethod().getName());
        GUtils.SendKeys(ni.txtFeeValue(), "0.0025");
    }




}
