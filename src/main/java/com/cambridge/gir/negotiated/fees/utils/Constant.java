package com.cambridge.gir.negotiated.fees.utils;

public class Constant {
    public static final String NegotiationSavedMessage = "Negotiation saved!";
    public static final String NegotiationDeletedMessage = "Negotiation deleted!";
    public static final String dateFormat = "MM/dd/yyyy";

}

