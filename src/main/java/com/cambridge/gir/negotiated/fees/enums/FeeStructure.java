package com.cambridge.gir.negotiated.fees.enums;

public enum FeeStructure {

    Flat ("Flat"),
    Tranches ("Tranches");

    private final String feeStructure;

    FeeStructure(String feeStructure) {
        this.feeStructure = feeStructure;
    }

    public String get() {
        return feeStructure;
    }
}
