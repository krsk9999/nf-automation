package com.cambridge.gir.negotiated.fees.entities;

import com.cambridge.gir.negotiated.fees.enums.FeeStructure;
import com.cambridge.gir.negotiated.fees.enums.FeeType;
import lombok.Data;

@Data
public class Fee {

    private FeeType feeType;
    private int holdingPeriod;
    private Boolean minimumMCAAUM;
    private Boolean minimumMClientNAV;
    private Double minimumM;
    private String feeDescription;
    /*Management Fee*/
    private Boolean investedCapital;
    private Boolean committedCapital;
    private Boolean insideCommitment;
    private Boolean onTopOfCommitment;
    private FeeStructure feeStructure;
    /*Flat*/
    private Double incentiveFeeCarriedInterest;
    /*Tranches*/
    private Boolean feeTypeCAAUM;
    private Boolean feeTypeClientNAV;
    private Boolean tranchesStepDown;
    private Boolean tranchesTiered;

    public Fee(){

    }

}
