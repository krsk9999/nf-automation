package com.cambridge.gir.negotiated.fees.entities;

import com.cambridge.gir.negotiated.fees.enums.EligibleClient;
import com.cambridge.gir.negotiated.fees.enums.NegotiationStatus;
import com.gft.qa.automation.core.utils.Constant;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
public class Negotiation {

    private EligibleClient eligibleClient;
    private String eligibleClientsInput;

    public String getEligibleClientsInput() {
        return eligibleClientsInput;
    }

    public void setEligibleClientsInput(String eligibleClientsInput) {
        this.eligibleClientsInput = eligibleClientsInput;
    }

    private LocalDate effectiveDate;
    private LocalDate endDate;
    private LocalDate closureDate;
    private NegotiationStatus negotiationStatus;
    private Boolean internalComments;
    private Boolean optica;
    private Boolean clientList;
    private Boolean newManager;
    private Boolean newLaunch;
    private List<String> negotiator;
    private String actionRequired;
    private String details;
    private List<Fee> fees;
    private List<Term> terms;
    private List<Tranch> tranches;

    public Negotiation(){
        fees = new ArrayList<>();
        terms = new ArrayList<>();
        negotiator = new ArrayList<>();
        tranches = new ArrayList<>();
    }

    public EligibleClient getEligibleClient() {
        return eligibleClient;
    }

    public void setEligibleClient(EligibleClient eligibleClient) {
        this.eligibleClient = eligibleClient;
    }

    public String getEffectiveDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.dateFormat);
        return this.effectiveDate.format(formatter);
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEndDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.dateFormat);
        return this.endDate.format(formatter);
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getClosureDate() {
        return closureDate;
    }

    public void setClosureDate(LocalDate closureDate) {
        this.closureDate = closureDate;
    }

    public NegotiationStatus getNegotiationStatus() {
        return negotiationStatus;
    }

    public void setNegotiationStatus(NegotiationStatus negotiationStatus) {
        this.negotiationStatus = negotiationStatus;
    }

    public Boolean getInternalComments() {
        return internalComments;
    }

    public void setInternalComments(Boolean internalComments) {
        this.internalComments = internalComments;
    }

    public Boolean getOptica() {
        return optica;
    }

    public void setOptica(Boolean optica) {
        this.optica = optica;
    }

    public Boolean getClientList() {
        return clientList;
    }

    public void setClientList(Boolean clientList) {
        this.clientList = clientList;
    }

    public Boolean getNewManager() {
        return newManager;
    }

    public void setNewManager(Boolean newManager) {
        this.newManager = newManager;
    }

    public Boolean getNewLaunch() {
        return newLaunch;
    }

    public void setNewLaunch(Boolean newLaunch) {
        this.newLaunch = newLaunch;
    }

    public List<String> getNegotiator() {
        return negotiator;
    }

    public void setNegotiator(List<String> negotiator) {
        this.negotiator = negotiator;
    }

    public void addNegotiator(String negotiator){
        this.negotiator.add(negotiator);
    }

    public String getActionRequired() {
        return actionRequired;
    }

    public void setActionRequired(String actionRequired) {
        this.actionRequired = actionRequired;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<Fee> getFees() {
        return fees;
    }

    public void setFees(List<Fee> fees) {
        this.fees = fees;
    }

    public void addFee(Fee fee) {
        this.fees.add(fee);
    }

    public void addTerm(Term term) {
        this.terms.add(term);
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }
}
