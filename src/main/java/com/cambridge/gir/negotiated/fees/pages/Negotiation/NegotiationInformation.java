package com.cambridge.gir.negotiated.fees.pages.Negotiation;

import com.cambridge.gir.negotiated.fees.enums.*;
import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.utils.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class NegotiationInformation {

    //<editor-fold desc="Selectors">

    private String cssEligibleClients = "select[name='eligibleClients']";
    private String cssNegotiationStatus = "select[name='negotiationStatus']";
    private String cssCheckBoxes = ".checks label";
    private String cssEligibleClientsInput = "tag-input[name='clients'] input";
    private String cssResultSetEligibleClients = "ng2-menu-item>div";
    private String cssStartDate = "#sd input";
    private String cssEndDate = "#ed input";
    private String cssSave = ".btn.btn-success";
    private String cssBtnDelete = "div.buttons button[data-target=\"#modalDelete\"]";
    private String cssBtnConfirmDelete = "button.btn.btn-danger";
    private String cssModalDialogDelete = ".modal-dialog";
    private String cssNegotiatiorInput = "tag-input[name=\"negotiator\"] tag-input-form > form > input";
    private String cssNegotiatorResultSet = "ng2-dropdown-menu ng2-menu-item";
    private String cssEligibleClientsResultSet = "ng2-dropdown-menu ng2-menu-item";
    private String cssActionRequiredToReceiveBenefit = "textarea[name=\"actionRequired\"]";
    private String cssDetails = "textarea[name=\"details\"]";
    private String xpAddFee = "//a[text()[contains(.,'Add Fee')]]";
    private String xpAddTerm = "//a[text()[contains(.,'Add Term')]]";
    private String cssFeeType = "select[name='feeType']";
    private String cssBtnOriginalFee = ".slide>input[type=\"checkbox\"]+label";
    private String cssFeeStructure = "select[name='feeStructure']";
    private String cssAddTranch = "div.add>a";
    private String idNewLaunch = "div.slide label";
    private String cssTxtFeeValue = "input[name=\"feeValue\"]";

    //</editor-fold>

    //<editor-fold desc="Properties">
    private List<WebElement> allCAClients;
    private Select eligibleClients;
    private Select negotiationStatus;
    private List<WebElement> negotiationAttributes;
    private Select feeType;
    private Select feeStructure;

    //</editor-fold>

    public NegotiationInformation() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssEligibleClients));
        WebElement selectEligibleClients = GUtils.findElement(By.cssSelector(this.cssEligibleClients));
        eligibleClients = new Select(selectEligibleClients);

        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssNegotiationStatus));
        WebElement selectNegotiationStatus = GUtils.findElement(By.cssSelector(this.cssNegotiationStatus));
        negotiationStatus = new Select(selectNegotiationStatus);

        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssCheckBoxes));
        negotiationAttributes = new ArrayList<>();
        negotiationAttributes = GUtils.findElements(By.cssSelector(this.cssCheckBoxes));
        //> input[name="flags"]
    }

    public void lstSelectEligibleClient(EligibleClient eligibleClient){
        this.eligibleClients.selectByVisibleText(eligibleClient.get());
    }

    public void lstSelectNegotiationStatus(NegotiationStatus negotiationStatus){
        this.negotiationStatus.selectByVisibleText(negotiationStatus.get());
    }

    public WebElement txtElegibleClientsInput() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssEligibleClientsInput));
        return GUtils.findElement(By.cssSelector(this.cssEligibleClientsInput));
    }

    public WebElement txtEligibleClientsResultSet() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssResultSetEligibleClients));
        return GUtils.findElement(By.cssSelector(this.cssResultSetEligibleClients));
    }

    public WebElement dpEffectiveDate() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssStartDate));
        return GUtils.findElement(By.cssSelector(this.cssStartDate));
    }

    public WebElement dpEndDate() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssEndDate));
        return GUtils.findElement(By.cssSelector(this.cssEndDate));
    }

    public WebElement chkNewLaunch() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.idNewLaunch));
        return GUtils.findElement(By.cssSelector(this.idNewLaunch));
    }

    public WebElement chkOriginalFee() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssBtnOriginalFee));
        return GUtils.findElements(By.cssSelector(this.cssBtnOriginalFee)).get(1);
    }

    public WebElement chkNegotiationAttribute(NegotiationAttributes negotiationAttribute) throws Exception{
        for(int i=0; i < this.negotiationAttributes.size() ; i++ ){

            String sValue = negotiationAttributes.get(i).getAttribute("innerText");
            if (sValue.equalsIgnoreCase(negotiationAttribute.get())){
                return negotiationAttributes.get(i).findElement(By.cssSelector("input"));

            }
        }
        return null;
    }

    public WebElement btnSave() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssSave));
        return GUtils.findElement(By.cssSelector(this.cssSave));
    }

    public WebElement btnDelete() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssBtnDelete));
        return GUtils.findElement(By.cssSelector(this.cssBtnDelete));
    }

    public WebElement btnConfirmDelete() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssBtnConfirmDelete));
        return GUtils.findElement(By.cssSelector(this.cssBtnConfirmDelete));
    }

    public WebElement waitForDeletionPrompt() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssModalDialogDelete));
        return GUtils.findElement(By.cssSelector(this.cssModalDialogDelete));
    }

    public WebElement txtNegotiator() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssNegotiatiorInput));
        return GUtils.findElement(By.cssSelector(this.cssNegotiatiorInput));
    }

    public void assignEligibleClient(List<String> eligibleClients) throws Exception{

        for (String eligibleClient: eligibleClients) {
            this.txtElegibleClientsInput().sendKeys(eligibleClient);
            Thread.sleep(1000);
            this.getEligibleClientResultSet().get(0).click();
        }
    }

    public List<WebElement> getEligibleClientResultSet() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssEligibleClientsResultSet));
        return GUtils.findElements(By.cssSelector(this.cssEligibleClientsResultSet));
    }

    public void assignNegotiator(List<String> negotiators) throws Exception{

        for (String negotiator: negotiators) {
            this.txtNegotiator().sendKeys(negotiator);
            Thread.sleep(1000);
            this.getNegotiatorResultSet().get(0).click();
        }
    }

    public List<WebElement> getNegotiatorResultSet() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssNegotiatorResultSet));
        return GUtils.findElements(By.cssSelector(this.cssNegotiatorResultSet));
    }

    public WebElement txtActionRequiredToReceiveBenefit() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssActionRequiredToReceiveBenefit));
        return GUtils.findElement(By.cssSelector(this.cssActionRequiredToReceiveBenefit));
    }

    public WebElement txtDetails() throws Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssDetails));
        return GUtils.findElement(By.cssSelector(this.cssDetails));
    }

    public WebElement btnAddFee() throws  Exception{
        GUtils.waitUntilElementIsDisplayed(By.xpath(this.xpAddFee));
        return GUtils.findElement(By.xpath(this.xpAddFee));
    }

    public WebElement btnAddTerm() throws  Exception{
        GUtils.waitUntilElementIsDisplayed(By.xpath(this.xpAddTerm));
        return GUtils.findElement(By.xpath(this.xpAddTerm));
    }

    public void lstSelectFeeType(FeeType feeType) throws Exception{

        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssFeeType));
        WebElement selectFeeType = GUtils.findElement(By.cssSelector(this.cssFeeType));
        this.feeType = new Select(selectFeeType);
        try{
            this.feeType.selectByVisibleText(feeType.get());
        }catch(Exception e){
            Log.error("There was an error selecting a Fee Type by visible text: " +feeType.get() );
            Log.error(e.getMessage());
        }
    }

    public void lstSelectFeeStructure(FeeStructure feeStructure) throws Exception{

        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssFeeStructure));
        WebElement selectFeeStructure = GUtils.findElement(By.cssSelector(this.cssFeeStructure));
        this.feeStructure = new Select(selectFeeStructure);
        try{
            this.feeStructure.selectByVisibleText(feeStructure.get());
        }catch(Exception e){
            Log.error("There was an error selecting a Fee Structure by visible text: " +feeStructure.get() );
            Log.error(e.getMessage());
        }
    }

    public WebElement btnAddTranch() throws  Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssAddTranch));
        return GUtils.findElement(By.cssSelector(this.cssAddTranch));
    }

    public WebElement txtFeeValue() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.cssTxtFeeValue));
        return GUtils.findElement(By.cssSelector(this.cssTxtFeeValue));
    }

}
