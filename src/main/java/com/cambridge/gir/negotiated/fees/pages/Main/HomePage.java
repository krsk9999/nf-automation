package com.cambridge.gir.negotiated.fees.pages.Main;

import com.cambridge.gir.negotiated.fees.enums.StatusLevel;
import com.cambridge.gir.negotiated.fees.utils.Constant;
import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.utils.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomePage {

    private String searchBox = "dynamicSearch";//id
    private String searchResults = "dynamic-search-item>a";
    private String addSearchResult = "div.action.align-self-center > span";
    private String addNegotiation = "a[href='javascript:void(0)']";
    private String addNegotiationText = "Add Negotiation";
    private String toast = ".toast";
    private String toastTitle = ".toast-title";
    private String toastMessage = ".toast-message";
    private String cssNegotiationSummaryTable = ".card-summary table.summary tr";

    public HomePage(){}

    public void Open() throws Exception {
        try {
            GUtils.driver.navigate().to(GUtils.properties.get(0).getProperty("baseUrl"));
            GUtils.driver.manage().window().maximize();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            GUtils.captureScreenShot(GUtils.driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public Map<Integer, WebElement> getNegotiationIdsFromSummaryTable() throws Exception {

        this.waitUntilNegotiationSummaryIsLoaded();

        List<WebElement> negotiations = GUtils.findElements(By.cssSelector(this.cssNegotiationSummaryTable));
        List<Integer> ids = new ArrayList<>();

        Map<Integer, WebElement> lsNegotiations = new HashMap<>();

        try {

            for (WebElement negotiation : negotiations) {
                String href = (negotiation.findElement(By.cssSelector("a.btn"))).getAttribute("href");
                String id = href.substring(href.lastIndexOf("/"), href.length()).replace("/", "");
                lsNegotiations.put(Integer.parseInt(id), negotiation);
            }
        } catch (Exception error) {

            Log.error("Error: " + error.getMessage());
        }

        return lsNegotiations;
    }

    public WebElement txtFirmSearchBox() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.id(this.searchBox));
        return GUtils.findElement(By.id(this.searchBox));
    }

    public List<WebElement> lstSearchResultSet() throws  Exception{
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.searchResults));
        return GUtils.findElements(By.cssSelector(this.searchResults));
    }

    public WebElement btnAddSearchResult() throws Exception{
        //TODO
        //WebElement test = (WebElement)GUtils.executeJquery("$(\"#dynamicSearch\").get(0);");
        // String texto = GUtils.executeJquery("$(\".navbar-brand\").get(0).text();").toString();
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.addSearchResult));
        List<WebElement> results = GUtils.findElements(By.cssSelector(this.addSearchResult));
        for (WebElement el:results) {
            if(GUtils.doesElementExist(By.xpath("//*[text()[contains(.,'ADD')]]"))){
                return  GUtils.findElement(By.xpath("//*[text()[contains(.,'ADD')]]"));
            }
        }
        return null;
    }

    public WebElement btnAddNegotiation() throws Exception{
        List<WebElement> links = GUtils.findElements(By.cssSelector(this.addNegotiation));
        WebElement element = null;
        for (WebElement link: links) {
            String text = link.getText();
            if (text.trim().equalsIgnoreCase(this.addNegotiationText)){
                element = link;
            }
        }
        return element;
    }

    public Boolean txtFirmSearchBoxExists() throws Exception {
        if(GUtils.isElementDisplayed(By.id(this.searchBox)))
            return true;
        else
            return false;
    }

    public Boolean waitUntilNegotiationSummaryIsLoaded() throws Exception{

        try{
            GUtils.waitForInvisibilityOfLoadingSpinner(By.cssSelector(".fa-spin"));
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public StatusLevel validateNegotiationSavedSuccessStatus() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.toast));
        WebElement toast = GUtils.findElement(By.cssSelector(this.toast));
        String toastTitle = toast.findElement(By.cssSelector(this.toastTitle)).getText();
        String toastMessage = toast.findElement(By.cssSelector(this.toastMessage)).getText();

        if (toastTitle.equalsIgnoreCase(StatusLevel.Success.get()) && toastMessage.equalsIgnoreCase(Constant.NegotiationSavedMessage)){
            return StatusLevel.Success;
        }else{
            return StatusLevel.Fail;
        }

    }

    public StatusLevel validateNegotiationDeletedSuccessStatus() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(this.toast));
        WebElement toast = GUtils.findElement(By.cssSelector(this.toast));
        String toastTitle = toast.findElement(By.cssSelector(this.toastTitle)).getText();
        String toastMessage = toast.findElement(By.cssSelector(this.toastMessage)).getText();

        if (toastMessage.equalsIgnoreCase(StatusLevel.Success.get()) && toastTitle.equalsIgnoreCase(Constant.NegotiationDeletedMessage)) {
            return StatusLevel.Success;
        } else {
            return StatusLevel.Fail;
        }

    }

    public WebElement btnEditNegotiation(WebElement negotiation) throws Exception {
        return negotiation.findElement(By.cssSelector("td a.btn"));
    }

    //toast

    /*try {
                AssertTrue(someCondition.isTrue());
            } catch (AssertionError ae) {
                log.info(some message);
            }
    */


}
