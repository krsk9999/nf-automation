package com.cambridge.gir.negotiated.fees.enums;

public enum TermType {

    AdvisoryCommittee ("Advisory Committee"),
    AllocationOfInvestmentOpportunities ("Allocation of Investment Opportunities"),
    DeemedConsent ("Deemed Consent"),
    DeemedOrSpecialCashlessGPContribution ("Deemed or Special, Cashless GP Contribution"),
    FundSizeHardCap ("Fund Size Hard Cap"),
    GPClawback ("GP Clawback"),
    GPCommitment ("GP Commitment"),
    Indemnification ("Indemnification"),
    KeyPerson ("Key Person"),
    LimitationsOfFiduciaryDuties ("Limitations of Fiduciary Duties"),
    LPClawback ("LP Clawback"),
    ManagementFeeOffsets ("Management Fee Offsets"),
    NoFaultTermination ("No-Fault Termination"),
    OrganizationalFees ("GP Commitment"),
    PartnershipExpenses ("Partnership Expenses"),
    Valuations ("Valuations"),
    ObserverSeat ("Observer Seat");

    private final String termType;

    TermType(String termType) {
        this.termType = termType;
    }

    public String get() {
        return termType;
    }
}
