package com.cambridge.gir.negotiated.fees.pages.Main;


import com.gft.qa.automation.core.utils.GUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import com.cambridge.gir.negotiated.fees.utils.Url;

public class TestPage {

    public TestPage() {
    }

    public void Open(){
        GUtils.driver.manage().window().maximize();
        String url = Url.generateURL();
        GUtils.driver.navigate().to(url);
    }

    public WebElement txtFirmSearchBox() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.id(""));
        return GUtils.findElement(By.id(""));
    }

    public List<WebElement> lstSearchResultSet() throws Exception {
        GUtils.waitUntilElementIsDisplayed(By.cssSelector(""));
        return GUtils.findElements(By.cssSelector(""));
    }

    //toast
    /*try {
                AssertTrue(someCondition.isTrue());
            } catch (AssertionError ae) {
                log.info(some message);
            }
    */


}
