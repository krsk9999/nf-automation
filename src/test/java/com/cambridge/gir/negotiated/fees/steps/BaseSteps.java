package com.cambridge.gir.negotiated.fees.steps;

import com.gft.qa.automation.core.utils.GUtils;
import org.openqa.selenium.WebDriver;

public class BaseSteps {

    protected static WebDriver driver = null;

    BaseSteps() {
        driver = GUtils.driver;
    }
}
