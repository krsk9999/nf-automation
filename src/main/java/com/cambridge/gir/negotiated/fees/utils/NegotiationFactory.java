package com.cambridge.gir.negotiated.fees.utils;

import com.cambridge.gir.negotiated.fees.entities.Fee;
import com.cambridge.gir.negotiated.fees.entities.Negotiation;
import com.cambridge.gir.negotiated.fees.entities.Term;
import com.cambridge.gir.negotiated.fees.entities.Tranch;
import com.cambridge.gir.negotiated.fees.enums.*;
import com.gft.qa.automation.core.utils.GExcelUtils;
import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.utils.Log;
import com.google.common.io.Resources;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NegotiationFactory {

    public static Negotiation createNegotiationObject() throws Exception{

        Negotiation negotiation;

        try{

            //GExcelUtils.setExcelFile("/src/test/resources/inputdata/SmokeTestingData.xlsx","Negotiation");
            //String sourceFile = GExcelUtils.getCellData(1,"startDate");


            /*Fees Creation*/
            Fee firstFee = new Fee();
            firstFee.setFeeType(FeeType.MANAGEMENTFEE);
            firstFee.setFeeStructure(FeeStructure.Tranches);
            firstFee.setHoldingPeriod(2);
            firstFee.setMinimumMCAAUM(true);
            firstFee.setMinimumM(25.50);
            firstFee.setInvestedCapital(true);
            firstFee.setInsideCommitment(true);
            /*Tranches table*/
            firstFee.setFeeTypeCAAUM(true);

            Tranch firstTranch = new Tranch(0D,100D, 0.005D);
            Tranch secondTranch = new Tranch(101D,200D,0.010D);
            Tranch thirdTranch = new Tranch(201D, 300D, 0.015D);

            List<Tranch> tranches = new ArrayList<>();
            tranches.add(firstTranch);
            tranches.add(secondTranch);
            tranches.add(thirdTranch);

            firstFee.setTranchesStepDown(true);
            firstFee.setFeeDescription("This is the first testing fee description");

            /*Term*/
            Term firstTerm = new Term();
            firstTerm.setTermType(TermType.GPCommitment);
            firstTerm.setAffectsLP(true);
            firstTerm.setHoldingPeriod(2);
            firstTerm.setMinimumM(26.60D);
            firstTerm.setMinimumMCAAUM(true);
            firstTerm.setGpCommitment(3.58);

            /*Negotiation*/
            Long ONE_YEAR = TimeUnit.DAYS.toMillis(365);
            negotiation = new Negotiation();
            negotiation.setEligibleClient(EligibleClient.ExcludeClientOrClientGroup);
            negotiation.setEligibleClientsInput("Hollins University");
            negotiation.setNewLaunch(false);
            negotiation.setEffectiveDate(LocalDate.now());
            negotiation.setEndDate(LocalDate.now().plusYears(1L));
            negotiation.setNegotiationStatus(NegotiationStatus.Active);
            negotiation.setInternalComments(true);
            negotiation.setOptica(true);
            negotiation.setClientList(true);
            negotiation.setNewManager(true);
            negotiation.addNegotiator(GUtils.properties.get(0).getProperty("Negotiator"));
            negotiation.addNegotiator("Jessica Kinner");
            negotiation.setActionRequired("No action is required");
            negotiation.setDetails("No details for this negotiation");



            //ADding Fees and Terms
            negotiation.addFee(firstFee);
            negotiation.setTranches(tranches);
            negotiation.addTerm(firstTerm);

        }catch(Exception e){
            throw e;
        }

        return negotiation;
    }

    public static List<Negotiation> createNegotiationsBasedOnExcelFile(String excelFileName, String excelSheet) throws Exception{

        List<Negotiation> list = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Constant.dateFormat);
        try{
            GExcelUtils.setExcelFile("/src/test/resources/inputdata/"+excelFileName,excelSheet);

            for(int i = 1; i <= GExcelUtils.getNumberOfRows(); i++){
                Negotiation ng = new Negotiation();
                Fee fee = new Fee();
                ng.setEffectiveDate(LocalDate.parse(GExcelUtils.getCellData(i,"startDate"),dateTimeFormatter));
                ng.setEndDate(LocalDate.parse(GExcelUtils.getCellData(i,"endDate"),dateTimeFormatter));
                fee.setFeeType(FeeType.valueOf(GExcelUtils.getCellData(i,"feeType")));
                ng.addFee(fee);
                list.add(ng);
            }

        }catch(Exception e){
            Log.error("Error found while setting up the input datasource file.\r\n"+e.getMessage());
        }

        return list;

    }


}
