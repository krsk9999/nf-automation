package com.cambridge.gir.negotiated.fees.entities;

import com.cambridge.gir.negotiated.fees.enums.TermType;

public class Term {

    private Boolean affectsLP;
    private int holdingPeriod;
    private Boolean minimumMCAAUM;
    private Boolean minimumMClientNAV;
    private Double minimumM;

    private TermType termType;
    /*GP Clawback*/
    private Double gpClowback;
    /*GP Commitment*/
    private Double gpCommitment;
    /*LP Clawback*/
    private Double lpClowback;
    /*Organizational Fees*/
    private Double organizationalFees;
    /*Partnership Expenses*/
    private Double partnershipExpenses;

    public Term() {
    }

    public Boolean getAffectsLP() {
        return affectsLP;
    }

    public void setAffectsLP(Boolean affectsLP) {
        this.affectsLP = affectsLP;
    }

    public int getHoldingPeriod() {
        return holdingPeriod;
    }

    public void setHoldingPeriod(int holdingPeriod) {
        this.holdingPeriod = holdingPeriod;
    }

    public Boolean getMinimumMCAAUM() {
        return minimumMCAAUM;
    }

    public void setMinimumMCAAUM(Boolean minimumMCAAUM) {
        this.minimumMCAAUM = minimumMCAAUM;
    }

    public Boolean getMinimumMClientNAV() {
        return minimumMClientNAV;
    }

    public void setMinimumMClientNAV(Boolean minimumMClientNAV) {
        this.minimumMClientNAV = minimumMClientNAV;
    }

    public Double getMinimumM() {
        return minimumM;
    }

    public void setMinimumM(Double minimumM) {
        this.minimumM = minimumM;
    }

    public TermType getTermType() {
        return termType;
    }

    public void setTermType(TermType termType) {
        this.termType = termType;
    }

    public Double getGpClowback() {
        return gpClowback;
    }

    public void setGpClowback(Double gpClowback) {
        this.gpClowback = gpClowback;
    }

    public Double getGpCommitment() {
        return gpCommitment;
    }

    public void setGpCommitment(Double gpCommitment) {
        this.gpCommitment = gpCommitment;
    }

    public Double getLpClowback() {
        return lpClowback;
    }

    public void setLpClowback(Double lpClowback) {
        this.lpClowback = lpClowback;
    }

    public Double getOrganizationalFees() {
        return organizationalFees;
    }

    public void setOrganizationalFees(Double organizationalFees) {
        this.organizationalFees = organizationalFees;
    }

    public Double getPartnershipExpenses() {
        return partnershipExpenses;
    }

    public void setPartnershipExpenses(Double partnershipExpenses) {
        this.partnershipExpenses = partnershipExpenses;
    }
}
