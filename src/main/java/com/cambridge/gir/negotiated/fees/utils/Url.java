package com.cambridge.gir.negotiated.fees.utils;

import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.exceptions.*;

public class Url {

    public static String generateURL() {

        String url = GUtils.properties.get(0).getProperty("baseUrl");
        String usernameSSO = GUtils.properties.get(0).getProperty("SSOUsername");
        String passwordSSO =  GUtils.properties.get(0).getProperty("SSOPassword");
        String domainUrl = GUtils.properties.get(0).getProperty("domainUrl");
        Boolean useSSO = Boolean.valueOf(GUtils.properties.get(0).getProperty("useSSO"));


            if (useSSO) {

                if(usernameSSO == null || usernameSSO.isEmpty()){
                    throw new UnknownConditionException("You are authenticating to the system via SSO; Therefore, the parameter \"SSOUsername\" cannot be null. Please check the applications.properties file.");
                }

                if(passwordSSO == null || passwordSSO.isEmpty()){
                    throw new UnknownConditionException("You are authenticating to the system via SSO; Therefore, the parameter \"SSOPassword\" cannot be null. Please check the applications.properties file.");
                }

                if(domainUrl == null || domainUrl.isEmpty()){
                    throw new UnknownConditionException("You are authenticating to the system via SSO; Therefore, the parameter \"domainUrl\" cannot be null. Please check the applications.properties file.");
                }

                url = "http://" + usernameSSO + ":" + passwordSSO + "@" + domainUrl;
            }

            if(url == null){
                throw new UnknownConditionException("The parameters \"baseUrl\" cannot be null. Please check the applications.properties file.");
            }

        return url;
    }

}
