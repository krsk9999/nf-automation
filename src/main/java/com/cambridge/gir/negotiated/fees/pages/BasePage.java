package com.cambridge.gir.negotiated.fees.pages;

import com.gft.qa.automation.core.utils.GUtils;
import org.openqa.selenium.WebDriver;

public class BasePage {

    protected static WebDriver driver = null;

    protected BasePage() {
        driver = GUtils.driver;
    }
}
