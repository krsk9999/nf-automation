package com.cambridge.gir.negotiated.fees.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/com/cambridge/gir/negotiated/fees/features/Test.feature"
        ,format = {"json:target/cucumber.json", "html:target/site/cucumber-pretty"}
        //,tags = {"@Regression"}
        ,glue = {"com.cambridge.gir.negotiated.fees.runners","src/test/java/com/cambridge/gir/negotiated/fees/steps",""}
)
public class TestRunner_Test extends AbstractTestNGCucumberTests {
        /**
         * Create one test method that will be invoked by TestNG and invoke the
         * Cucumber runner within that method.
         */
        public void runCukes() {}
}
