package com.cambridge.gir.negotiated.fees.enums;

public enum StatusLevel {

    Success ("Success"),
    Error ("Error"),
    Pass ("Pass"),
    Fail ("Fail");

    private final String statusLevel;

    StatusLevel(String statusLevel) {
        this.statusLevel = statusLevel;
    }

    public String get() {
        return statusLevel;
    }

}
